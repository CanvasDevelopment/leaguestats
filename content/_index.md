## LeagueStats

LeagueStats is an android application designed to help users measure their League of Legends performance against 
their lane opponent, regardless of game result.

It allows the user to view specific stats such as Creep Score, Kills, Deaths, XP, Gold for each match, as well as an overall average (e.g last 20 games)

These stats are then displayed side by side with your opponent (the enemy laner) to get an indication of who performed best across each stat.

Stats can be broken down by game stage, as well as filtered by champ and role type to make these statistics more useful.

LeagueStats isn’t endorsed by Riot Games and doesn’t reflect the views or opinions of Riot Games or anyone officially involved in producing or managing League of Legends. League of Legends and Riot Games are trademarks or registered trademarks of Riot Games, Inc. League of Legends © Riot Games, Inc.
